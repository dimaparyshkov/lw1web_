// Подключение всех модулей к программе
var express = require('express');
var app = express();
app.use(express.static('./'));
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');

// Отслеживание порта
server.listen(8080);
console.log("Running on http://localhost:8080");

// Отслеживание url адреса и отображение нужной HTML страницы
app.get('/', function(request, respons) {
	respons.sendFile(__dirname + '/web.html');
});

messagesTest = [];

let fileContent = fs.readFileSync("./comments.txt", "utf8");
var textByLine = fileContent.split("\n")
for (i = 0; i<textByLine.length;i+=2) {
    messagesTest.push({
        mess: textByLine[i],
        name: textByLine[i+1],
        className: 'secondary'
    });
}

// Массив со всеми подключениями
connections = [];

// Функция, которая сработает при подключении к странице
// Считается как новый пользователь
io.sockets.on('connection', function(socket) {
	console.log("Успешное соединение");
	// Добавление нового соединения в массив
	connections.push(socket);
	
	    for (var i = 0; i < messagesTest.length; i++) {
        io.to(socket.id).emit('add mess', {mess: messagesTest[i].mess, name: messagesTest[i].name, className: 'secondary'});
    }
	
	    socket.on('loadAllMess', function(data) {
        console.log('Доабвлены все сообщения');
        console.log('Всего сообщений ' + messagesTest.length);
        for (var i = 0; i < messagesTest.length; i++) {
            io.sockets.emit('add mess', {mess: messagesTest[i].mess, name: messagesTest[i].name, className: 'secondary'});
        }
    });


	// Функция, которая срабатывает при отключении от сервера
	socket.on('disconnect', function(data) {
		// Удаления пользователя из массива
		connections.splice(connections.indexOf(socket), 1);
		console.log("Отключились");
	});

	// Функция получающая сообщение от какого-либо пользователя
	socket.on('send mess', function(data) {
		 console.log('Nem mess');
        messagesTest.push({name: data.name,
            mess: data.mess,
            className: data.className});
        fs.appendFileSync("./comments.txt", "\n" + data.mess + "\n" + data.name);
		// Внутри функции мы передаем событие 'add mess',
		// которое будет вызвано у всех пользователей и у них добавиться новое сообщение
		io.sockets.emit('add mess', {mess: data.mess, name: data.name, className: data.className});
	});

});
